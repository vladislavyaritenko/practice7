package ua.nure.yaritenko.Practice7;

import ua.nure.yaritenko.Practice7.controller.DOMController;
import ua.nure.yaritenko.Practice7.controller.SAXController;
import ua.nure.yaritenko.Practice7.controller.STAXController;
import ua.nure.yaritenko.Practice7.entity.Papers;
import ua.nure.yaritenko.Practice7.util.Sort;

public class Main {
    public static void usage() {
        System.out.println("Usage:\njava -jar Practice7.jar xmlFileName");
        System.out.println("java ua.nure.yaritenko.Practice7.Main xmlFileName");
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            usage();
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get
        DOMController domController = new DOMController(xmlFileName);
        domController.parse(true);
        Papers papers = domController.getPapers();

        // sort (case 1)
        Sort.sortPaperBySize(papers);

        // save
        String outputXmlFile = "Papers.xml.dom-result.xml";
        DOMController.saveToXML(papers, outputXmlFile);
        System.out.println("Output ==> " + outputXmlFile);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        saxController.parse(true);
        papers = saxController.getPapers();

        // sort  (case 2)
        Sort.setSortPaperByTitel(papers);

        // save
        outputXmlFile = "Papers.xml.sax-result.xml";

        // other way:
        DOMController.saveToXML(papers, outputXmlFile);
        System.out.println("Output ==> " + outputXmlFile);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        staxController.parse();
        papers = staxController.getPapers();

        // sort  (case 3)
        Sort.sortPaperByType(papers);

        // save
        outputXmlFile = "Papers.xml.stax-result.xml";
        DOMController.saveToXML(papers, outputXmlFile);
        System.out.println("Output ==> " + outputXmlFile);
    }
}
