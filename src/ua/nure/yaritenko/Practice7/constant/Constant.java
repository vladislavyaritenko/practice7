package ua.nure.yaritenko.Practice7.constant;

public final class Constant {
    // files
    public static final String VALID_XML_FILE = "Papers.xml";
    public static final String INVALID_XML_FILE = "Papers-invalid.xml";
    public static final String XSD_FILE = "Papers.xsd";

    // validation features
    public static final String FEATURE_TURN_VALIDATION_ON = "http://xml.org/sax/features/validation";
    public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON = "http://apache.org/xml/features/validation/schema";
}
