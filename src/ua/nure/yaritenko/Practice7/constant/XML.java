package ua.nure.yaritenko.Practice7.constant;

public enum XML {
    PAPERS("papers"), PAPER("paper"), TITLE("titel"), MOTHLY("monthly"), COLOR("color"), SIZE("size"), GLOSSY("glossy"), INDEX("index"),

    // attribute name
    TYPE("xsi:type");

    private String value;
    XML(String value) {
        this.value = value;
    }
    public boolean equalsTo(String name) {
        return value.equals(name);
    }
    public String getValue() {
        return value;
    }
}
