package ua.nure.yaritenko.Practice7.controller;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import ua.nure.yaritenko.Practice7.constant.Constant;
import ua.nure.yaritenko.Practice7.constant.XML;
import ua.nure.yaritenko.Practice7.entity.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

public class DOMController {
    private String xmlFileName;
    private Papers papers;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Papers getPapers() {
        return papers;
    }

    public void parse(boolean validate) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        if (validate) {
            dbf.setFeature(Constant.FEATURE_TURN_VALIDATION_ON, true);
            dbf.setFeature(Constant.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }
        DocumentBuilder db = dbf.newDocumentBuilder();
        db.setErrorHandler(new DefaultHandler() {
            @Override
            public void error(SAXParseException e) throws SAXException {
                throw e;
            }
        });
        Document document = db.parse(xmlFileName);
        Element root = document.getDocumentElement();
        papers = new Papers();
        NodeList paperNodes = root.getElementsByTagName(XML.PAPER.getValue());
        for (int j = 0; j < paperNodes.getLength(); j++) {
            Paper paper = getPaper2(paperNodes.item(j));
            papers.getPaper().add(paper);
        }
    }

    private Paper getPaper2(Node pNode) {
        Paper paper = new Paper();
        Element pElement = (Element) pNode;
        paper.setType(pElement.getAttribute(XML.TYPE.getValue()).substring(4));
        String type = pElement.getAttribute(XML.TYPE.getValue()).substring(4);
        Node ptNode;
        switch (paper.getType()) {
            case "magazine":
                paper = new Magazine();
                paper.setType(type);
                ptNode = pElement.getElementsByTagName(XML.TITLE.getValue()).item(0);
                paper.setTitel(ptNode.getTextContent());
                ptNode = pElement.getElementsByTagName(XML.MOTHLY.getValue()).item(0);
                paper.setMonthly(Boolean.parseBoolean(ptNode.getTextContent()));
                ptNode = pElement.getElementsByTagName(XML.COLOR.getValue()).item(0);
                paper.setColor(Boolean.parseBoolean(ptNode.getTextContent()));
                ptNode = pElement.getElementsByTagName(XML.SIZE.getValue()).item(0);
                paper.setSize(Integer.parseInt(ptNode.getTextContent()));
                ptNode = pElement.getElementsByTagName(XML.GLOSSY.getValue()).item(0);
                ((Magazine) paper).setGlossy(Boolean.parseBoolean(ptNode.getTextContent()));
                ptNode = pElement.getElementsByTagName(XML.INDEX.getValue()).item(0);
                ((Magazine) paper).setIndex(Integer.parseInt(ptNode.getTextContent()));
                return paper;
            case "newspaper":
                paper = new Newspaper();
                paper.setType(type);
                ptNode = pElement.getElementsByTagName(XML.TITLE.getValue()).item(0);
                paper.setTitel(ptNode.getTextContent());
                ptNode = pElement.getElementsByTagName(XML.MOTHLY.getValue()).item(0);
                paper.setMonthly(Boolean.parseBoolean(ptNode.getTextContent()));
                ptNode = pElement.getElementsByTagName(XML.COLOR.getValue()).item(0);
                paper.setColor(Boolean.parseBoolean(ptNode.getTextContent()));
                ptNode = pElement.getElementsByTagName(XML.SIZE.getValue()).item(0);
                paper.setSize(Integer.parseInt(ptNode.getTextContent()));
                ptNode = pElement.getElementsByTagName(XML.INDEX.getValue()).item(0);
                ((Newspaper) paper).setIndex(Integer.parseInt(ptNode.getTextContent()));
                return paper;
            case "booklet":
                paper = new Booklet();
                paper.setType(type);
                ptNode = pElement.getElementsByTagName(XML.TITLE.getValue()).item(0);
                paper.setTitel(ptNode.getTextContent());
                ptNode = pElement.getElementsByTagName(XML.MOTHLY.getValue()).item(0);
                paper.setMonthly(Boolean.parseBoolean(ptNode.getTextContent()));
                ptNode = pElement.getElementsByTagName(XML.COLOR.getValue()).item(0);
                paper.setColor(Boolean.parseBoolean(ptNode.getTextContent()));
                ptNode = pElement.getElementsByTagName(XML.SIZE.getValue()).item(0);
                paper.setSize(Integer.parseInt(ptNode.getTextContent()));
                ptNode = pElement.getElementsByTagName(XML.GLOSSY.getValue()).item(0);
                ((Booklet) paper).setGlossy(Boolean.parseBoolean(ptNode.getTextContent()));
                return paper;
        }
        return null;
    }

    public static Document getDocument(Papers papers) throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.newDocument();
        Element tElement = document.createElement(XML.PAPERS.getValue());
        document.appendChild(tElement);
        for (Paper paper2 : papers.getPaper()) {
            Element pElement = document.createElement(XML.PAPER.getValue());
            switch (paper2.getType()) {
                case "magazine": {
                    pElement.setAttribute(XML.TYPE.getValue().substring(4), "magazine");
                    tElement.appendChild(pElement);
                    Element ptElement = document.createElement(XML.TITLE.getValue());
                    ptElement.setTextContent(paper2.getTitel());
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.MOTHLY.getValue());
                    ptElement.setTextContent(String.valueOf(paper2.getMonthly()));
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.COLOR.getValue());
                    ptElement.setTextContent(String.valueOf(paper2.getColor()));
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.SIZE.getValue());
                    ptElement.setTextContent(String.valueOf(paper2.getSize()));
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.GLOSSY.getValue());
                    ptElement.setTextContent(String.valueOf(((Magazine) paper2).getGlossy()));
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.INDEX.getValue());
                    ptElement.setTextContent(String.valueOf(((Magazine) paper2).getIndex()));
                    pElement.appendChild(ptElement);
                    break;
                }
                case "newspaper": {
                    pElement.setAttribute(XML.TYPE.getValue().substring(4), "newspaper");
                    tElement.appendChild(pElement);
                    Element ptElement = document.createElement(XML.TITLE.getValue());
                    ptElement.setTextContent(paper2.getTitel());
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.MOTHLY.getValue());
                    ptElement.setTextContent(String.valueOf(paper2.getMonthly()));
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.COLOR.getValue());
                    ptElement.setTextContent(String.valueOf(paper2.getColor()));
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.SIZE.getValue());
                    ptElement.setTextContent(String.valueOf(paper2.getSize()));
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.INDEX.getValue());
                    ptElement.setTextContent(String.valueOf(((Newspaper) paper2).getIndex()));
                    pElement.appendChild(ptElement);
                    break;
                }
                case "booklet": {
                    pElement.setAttribute(XML.TYPE.getValue().substring(4), "booklet");
                    tElement.appendChild(pElement);
                    Element ptElement = document.createElement(XML.TITLE.getValue());
                    ptElement.setTextContent(paper2.getTitel());
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.MOTHLY.getValue());
                    ptElement.setTextContent(String.valueOf(paper2.getMonthly()));
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.COLOR.getValue());
                    ptElement.setTextContent(String.valueOf(paper2.getColor()));
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.SIZE.getValue());
                    ptElement.setTextContent(String.valueOf(paper2.getSize()));
                    pElement.appendChild(ptElement);
                    ptElement = document.createElement(XML.GLOSSY.getValue());
                    ptElement.setTextContent(String.valueOf(((Booklet) paper2).getGlossy()));
                    pElement.appendChild(ptElement);
                    break;
                }
            }
        }
        return document;
    }

    public static void saveToXML(Papers papers, String xmlFileName)
            throws ParserConfigurationException, TransformerException {
        saveToXML(getDocument(papers), xmlFileName);
    }

    public static void saveToXML(Document document, String xmlFileName)
            throws TransformerException {
        StreamResult result = new StreamResult(new File(xmlFileName));
        TransformerFactory tf = TransformerFactory.newInstance();
        javax.xml.transform.Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.transform(new DOMSource(document), result);
    }

    public static void main(String[] args) throws Exception {
        DOMController domContr = new DOMController(Constant.VALID_XML_FILE);
        try {
            domContr.parse(true);
        } catch (SAXException ex) {
            System.err.println("====================================");
            System.err.println("XML not valid");
            System.err.println("Papers object --> " + domContr.getPapers());
            System.err.println("====================================");
        }
        domContr.parse(false);
        System.out.println("====================================");
        System.out.print("Here is the paper: \n" + domContr.getPapers());
        System.out.println("====================================");
        Papers papers = domContr.getPapers();
        DOMController.saveToXML(papers, Constant.VALID_XML_FILE + ".dom-result.xml");
    }
}
