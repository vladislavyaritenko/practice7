package ua.nure.yaritenko.Practice7.controller;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ua.nure.yaritenko.Practice7.constant.Constant;
import ua.nure.yaritenko.Practice7.constant.XML;
import ua.nure.yaritenko.Practice7.entity.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class SAXController extends DefaultHandler {
    private String xmlFileName;
    private String currentElement;
    private Papers papers;
    private Magazine magazine;
    private Booklet booklet;
    private Newspaper newspaper;
    private String type;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parse(boolean validate)
            throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        if (validate) {
            factory.setFeature(Constant.FEATURE_TURN_VALIDATION_ON, true);
            factory.setFeature(Constant.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }
        SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
    }

    @Override
    public void error(org.xml.sax.SAXParseException e) throws SAXException {
        throw e;
    }

    public Papers getPapers() {
        return papers;
    }

    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {
        currentElement = localName;
        if (XML.PAPERS.equalsTo(currentElement)) {
            papers = new Papers();
            return;
        }
        if (XML.PAPER.equalsTo(currentElement)) {
            if (attributes.getLength() > 0) {
                type = attributes.getValue(XML.TYPE.getValue()).substring(4);
            }
            switch (type) {
                case "magazine":
                    magazine = new Magazine();
                    magazine.setType(type);
                    return;
                case "newspaper":
                    newspaper = new Newspaper();
                    newspaper.setType(type);
                    return;
                case "booklet":
                    booklet = new Booklet();
                    booklet.setType(type);
                    return;
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        String elementText = new String(ch, start, length).trim();
        if (elementText.isEmpty()) {
            return;
        }
        switch (type) {
            case "magazine":
                if (XML.TITLE.equalsTo(currentElement)) {
                    magazine.setTitel(elementText);
                    return;
                }
                if (XML.MOTHLY.equalsTo(currentElement)) {
                    magazine.setMonthly(Boolean.parseBoolean(elementText));
                    return;
                }
                if (XML.COLOR.equalsTo(currentElement)) {
                    magazine.setColor(Boolean.parseBoolean(elementText));
                    return;
                }
                if (XML.SIZE.equalsTo(currentElement)) {
                    magazine.setSize(Integer.parseInt(elementText));
                    return;
                }
                if (XML.GLOSSY.equalsTo(currentElement)) {
                    magazine.setGlossy(Boolean.parseBoolean(elementText));
                    return;
                }
                if (XML.INDEX.equalsTo(currentElement)) {
                    magazine.setIndex(Integer.parseInt(elementText));
                    return;
                }
                break;
            case "newspaper":
                if (XML.TITLE.equalsTo(currentElement)) {
                    newspaper.setTitel(elementText);
                    return;
                }
                if (XML.MOTHLY.equalsTo(currentElement)) {
                    newspaper.setMonthly(Boolean.parseBoolean(elementText));
                    return;
                }
                if (XML.COLOR.equalsTo(currentElement)) {
                    newspaper.setColor(Boolean.parseBoolean(elementText));
                    return;
                }
                if (XML.SIZE.equalsTo(currentElement)) {
                    newspaper.setSize(Integer.parseInt(elementText));
                    return;
                }
                if (XML.INDEX.equalsTo(currentElement)) {
                    newspaper.setIndex(Integer.parseInt(elementText));
                    return;
                }
                break;
            case "booklet":
                if (XML.TITLE.equalsTo(currentElement)) {
                    booklet.setTitel(elementText);
                    return;
                }
                if (XML.MOTHLY.equalsTo(currentElement)) {
                    booklet.setMonthly(Boolean.parseBoolean(elementText));
                    return;
                }
                if (XML.COLOR.equalsTo(currentElement)) {
                    booklet.setColor(Boolean.parseBoolean(elementText));
                    return;
                }
                if (XML.SIZE.equalsTo(currentElement)) {
                    booklet.setSize(Integer.parseInt(elementText));
                    return;
                }
                if (XML.GLOSSY.equalsTo(currentElement)) {
                    booklet.setGlossy(Boolean.parseBoolean(elementText));
                    return;
                }
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        if (XML.PAPER.equalsTo(localName)) {
            switch (type) {
                case "magazine":
                    papers.getPaper().add(magazine);
                    return;
                case "newspaper":
                    papers.getPaper().add(newspaper);
                    return;
                case "booklet":
                    papers.getPaper().add(booklet);
                    return;
            }
        }
    }

    public static void main(String[] args) throws Exception {
        SAXController saxContr = new SAXController(Constant.VALID_XML_FILE);
        saxContr.parse(true);
        Papers papers = saxContr.getPapers();
        System.out.println("====================================");
        System.out.print("Here is the papers: \n" + papers);
        System.out.println("====================================");
        saxContr = new SAXController(Constant.INVALID_XML_FILE);
        try {
            saxContr.parse(true);
        } catch (Exception ex) {
            System.err.println("====================================");
            System.err.println("Validation is failed:\n" + ex.getMessage());
            System.err.println("Try to print test object:" + saxContr.getPapers());
            System.err.println("====================================");
        }
        saxContr.parse(false);
        System.out.println("====================================");
        System.out.print("Here is the papers: \n" + saxContr.getPapers());
        System.out.println("====================================");
    }
}
