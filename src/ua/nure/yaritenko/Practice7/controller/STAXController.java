package ua.nure.yaritenko.Practice7.controller;

import org.xml.sax.SAXException;
import ua.nure.yaritenko.Practice7.constant.Constant;
import ua.nure.yaritenko.Practice7.constant.XML;
import ua.nure.yaritenko.Practice7.entity.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.util.Iterator;

public class STAXController {
    private String xmlFileName;
    private Papers papers;
    String type;
    public Papers getPapers() {
        return papers;
    }
    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }
    public void parse() throws ParserConfigurationException, SAXException,
            IOException, XMLStreamException {
        Magazine magazine = null;
        Newspaper newspaper = null;
        Booklet booklet = null;
        String currentElement = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
        XMLEventReader reader = factory.createXMLEventReader(
                new StreamSource(xmlFileName));
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
                continue;
            }
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                currentElement = startElement.getName().getLocalPart();
                if (XML.PAPERS.equalsTo(currentElement)) {
                    papers = new Papers();
                    continue;
                }
                if (XML.PAPER.equalsTo(currentElement)) {
                    Attribute attribute = null;
                    for (Iterator i = startElement.getAttributes(); i.hasNext();) {
                        attribute = (Attribute) i.next();
                    }
                    type = attribute.getValue().substring(4);
                    switch (type) {
                        case "magazine":
                            magazine = new Magazine();
                            magazine.setType(type);
                            continue;
                        case "newspaper":
                            newspaper = new Newspaper();
                            newspaper.setType(type);
                            continue;
                        case "booklet":
                            booklet = new Booklet();
                            booklet.setType(type);
                           continue;
                    }
                }
            }
            if (event.isCharacters()) {
                Characters characters = event.asCharacters();
                switch (type) {
                    case "magazine":
                        if (XML.TITLE.equalsTo(currentElement)) {
                            magazine.setTitel(characters.getData());
                            continue;
                        }
                        if (XML.MOTHLY.equalsTo(currentElement)) {
                            magazine.setMonthly(Boolean.parseBoolean(characters.getData()));
                            continue;
                        }
                        if (XML.COLOR.equalsTo(currentElement)) {
                            magazine.setColor(Boolean.parseBoolean(characters.getData()));
                            continue;
                        }
                        if (XML.SIZE.equalsTo(currentElement)) {
                            magazine.setSize(Integer.parseInt(characters.getData()));
                            continue;
                        }
                        if (XML.GLOSSY.equalsTo(currentElement)) {
                            magazine.setGlossy(Boolean.parseBoolean(characters.getData()));
                            continue;
                        }
                        if (XML.INDEX.equalsTo(currentElement)) {
                            magazine.setIndex(Integer.parseInt(characters.getData()));
                            continue;
                        }
                        break;
                    case "newspaper":
                        if (XML.TITLE.equalsTo(currentElement)) {
                            newspaper.setTitel(characters.getData());
                            continue;
                        }
                        if (XML.MOTHLY.equalsTo(currentElement)) {
                            newspaper.setMonthly(Boolean.parseBoolean(characters.getData()));
                            continue;
                        }
                        if (XML.COLOR.equalsTo(currentElement)) {
                            newspaper.setColor(Boolean.parseBoolean(characters.getData()));
                            continue;
                        }
                        if (XML.SIZE.equalsTo(currentElement)) {
                            newspaper.setSize(Integer.parseInt(characters.getData()));
                            continue;
                        }
                        if (XML.INDEX.equalsTo(currentElement)) {
                            newspaper.setIndex(Integer.parseInt(characters.getData()));
                            continue;
                        }
                        break;
                    case "booklet":
                        if (XML.TITLE.equalsTo(currentElement)) {
                            booklet.setTitel(characters.getData());
                            continue;
                        }
                        if (XML.MOTHLY.equalsTo(currentElement)) {
                            booklet.setMonthly(Boolean.parseBoolean(characters.getData()));
                            continue;
                        }
                        if (XML.COLOR.equalsTo(currentElement)) {
                            booklet.setColor(Boolean.parseBoolean(characters.getData()));
                            continue;
                        }
                        if (XML.SIZE.equalsTo(currentElement)) {
                            booklet.setSize(Integer.parseInt(characters.getData()));
                            continue;
                        }
                        if (XML.GLOSSY.equalsTo(currentElement)) {
                            booklet.setGlossy(Boolean.parseBoolean(characters.getData()));
                            continue;
                        }
                        break;
                }
            }
            if (event.isEndElement()) {
                EndElement endElement = event.asEndElement();
                String localName = endElement.getName().getLocalPart();
                if (XML.PAPER.equalsTo(localName)) {
                    switch (type) {
                        case "magazine":
                            papers.getPaper().add(magazine);
                            continue;
                        case "newspaper":
                            papers.getPaper().add(newspaper);
                            continue;
                        case "booklet":
                            papers.getPaper().add(booklet);
                            continue;
                    }
                }
            }
        }
        reader.close();
    }
    public static void main(String[] args) throws Exception {
        STAXController staxContr = new STAXController(Constant.VALID_XML_FILE);
        staxContr.parse();
        Papers papers = staxContr.getPapers();
        System.out.println("====================================");
        System.out.print("Here is the papers: \n" + papers);
        System.out.println("====================================");
    }
}
