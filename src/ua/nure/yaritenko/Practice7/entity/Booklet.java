package ua.nure.yaritenko.Practice7.entity;

public class Booklet extends Paper {
    private Boolean glossy;
    private static final String EOL = System.lineSeparator();

    public Boolean getGlossy() {
        return glossy;
    }

    public void setGlossy(Boolean glossy) {
        this.glossy = glossy;
    }


    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(super.toString());
        result.append(glossy).append(EOL);
        return result.toString();
    }
}
