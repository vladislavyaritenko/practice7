package ua.nure.yaritenko.Practice7.entity;

public class Magazine extends Paper {
    private Boolean glossy;
    private int index;
    private static final String EOL = System.lineSeparator();

    public Boolean getGlossy() {
        return glossy;
    }

    public void setGlossy(Boolean glossy) {
        this.glossy = glossy;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }



    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(super.toString());
        result.append(glossy).append(EOL);
        result.append(index).append(EOL);
        return result.toString();
    }
}
