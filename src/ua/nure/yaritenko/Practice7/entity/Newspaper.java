package ua.nure.yaritenko.Practice7.entity;


public class Newspaper extends Paper {
    private int index;
    private static final String EOL = System.lineSeparator();


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(super.toString());
        result.append(index).append(EOL);
        return result.toString();
    }
}

