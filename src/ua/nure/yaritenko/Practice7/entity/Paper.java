package ua.nure.yaritenko.Practice7.entity;

public class Paper {
    private String titel;
    private Boolean monthly;
    private Boolean color;
    private int size;
    private String type;

    private static final String EOL = System.lineSeparator();

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public Boolean getMonthly() {
        return monthly;
    }

    public void setMonthly(Boolean monthly) {
        this.monthly = monthly;
    }

    public Boolean getColor() {
        return color;
    }

    public void setColor(Boolean color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isType(String t) {
        if (type.equals(t)) {
            return true;
        }
        return false;
    }


    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(type).append(EOL);
        result.append(titel).append(EOL);
        result.append(monthly).append(EOL);
        result.append(color).append(EOL);
        result.append(size).append(EOL);

        return result.toString();
    }


}
