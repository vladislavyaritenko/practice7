package ua.nure.yaritenko.Practice7.entity;

import java.util.ArrayList;
import java.util.List;

public class Papers {
    private List<Paper> papers;

    public List<Paper> getPaper() {
        if (papers == null) {
            papers = new ArrayList <>();
        }
        return papers;
    }

    public String toString() {
        if (papers == null || papers.size() == 0) {
            return "Периодические издания отсутствуют";
        }
        StringBuilder result = new StringBuilder();
        for (Paper paper : papers) {
            result.append(paper).append(System.lineSeparator());
        }
        return result.toString();
    }


}
