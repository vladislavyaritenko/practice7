package ua.nure.yaritenko.Practice7.util;

import ua.nure.yaritenko.Practice7.constant.Constant;
import ua.nure.yaritenko.Practice7.controller.DOMController;
import ua.nure.yaritenko.Practice7.entity.Paper;
import ua.nure.yaritenko.Practice7.entity.Papers;

import java.util.Collections;
import java.util.Comparator;

public class Sort {
    public static final Comparator<Paper> SORT_PAPER_BY_SIZE = new Comparator<Paper>() {
        @Override
        public int compare(Paper o1, Paper o2) {
            return o1.getSize() - o2.getSize();
        }
    };

    /**
     * Sorts questions by answers number.
     */
    public static final Comparator<Paper> SORT_PAPER_BY_TYPE = new Comparator<Paper>() {
        @Override
        public int compare(Paper o1, Paper o2) {
            return o1.getType().compareTo(o2.getType());
        }
    };

    /**
     * Sorts answers.
     */
    public static final Comparator<Paper> SORT_PAPER_BY_TITEL = new Comparator<Paper>() {
        @Override
        public int compare(Paper o1, Paper o2) {
            return o1.getTitel().compareTo(o2.getTitel());
        }
    };

    // //////////////////////////////////////////////////////////
    // these methods take Test object and sort it
    // with according comparator
    // //////////////////////////////////////////////////////////

    public static final void sortPaperBySize(Papers papers) {
            Collections.sort(papers.getPaper(), SORT_PAPER_BY_SIZE);
    }

    public static final void sortPaperByType(Papers papers) {
        Collections.sort(papers.getPaper(), SORT_PAPER_BY_TYPE);
    }

    public static final void setSortPaperByTitel(Papers papers) {
            Collections.sort(papers.getPaper(), SORT_PAPER_BY_TITEL);
    }


    public static void main(String[] args) throws Exception {
        // Test.xml --> Test object
        DOMController domController = new DOMController(
                Constant.VALID_XML_FILE);
        domController.parse(false);
        Papers papers = domController.getPapers();

        System.out.println("====================================");
        System.out.println(papers);
        System.out.println("====================================");

        System.out.println("====================================");
        Sort.sortPaperBySize(papers);
        System.out.println(papers);
        System.out.println("====================================");
    }
}
